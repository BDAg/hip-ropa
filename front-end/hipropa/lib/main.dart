import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:hipropa/blocs/product.dart';
import 'package:hipropa/blocs/user.dart';
import 'package:hipropa/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: UserBloc(),
      child: BlocProvider(
        bloc: ProductBloc(),
        child: MaterialApp(
          title: 'Hip-Ropa',
          routes: routes,
        )
      )
    );
  }
}