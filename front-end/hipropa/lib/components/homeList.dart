import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:hipropa/blocs/product.dart';
import 'package:hipropa/models/category.dart';
import 'package:hipropa/pages/product.dart';
import 'package:hipropa/pages/category.dart';

class HomeList extends StatefulWidget {
  @override
  _HomeListState createState() => new _HomeListState();
}

class _HomeListState extends State<HomeList> {

  ProductBloc blocProduct;

  @override
  Widget build(BuildContext context) {
    this.blocProduct = BlocProvider.of<ProductBloc>(context);

    return Scaffold(
      body: Container(
        color: const Color(0xffF4F7FA),
        child: ListView(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 16, top: 4),
                  child: Text(
                    'Todas Categorias',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16, top: 4),
                  child: FlatButton(
                    onPressed: () {},
                    child: Text(
                      'View All',
                      style: TextStyle(color: Colors.green),
                    ),
                  ),
                ),
              ],
            ),
            _buildCategoryList(),
            StreamBuilder(
              stream: this.blocProduct.productsStream,
              initialData: false,
              builder: (context, snapshot) {
                return FutureBuilder(
                  future: this.blocProduct.getCategoriesOFF(),
                  initialData: [],
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                      case ConnectionState.none:
                        return Container(
                          alignment: Alignment.center,
                          width: 200.0,
                          height: 200.0,
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white),
                            strokeWidth: 5.0,
                          ),
                        );
                      default:
                        if (snapshot.hasError)
                          return Container();
                        else {
                          return Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 16, top: 4),
                                    child: Text(
                                      'Ofertas de ${snapshot.data[0].name}',
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 16, top: 4),
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) {
                                              return CategoryPage(snapshot.data[0]);
                                            }
                                          )
                                        );
                                      },
                                      child: Text(
                                        'View All',
                                        style: TextStyle(color: Colors.green),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              _buildProductsCategory(snapshot.data[0]),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 16, top: 4),
                                    child: Text(
                                      'Ofertas de ${snapshot.data[1].name}',
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 16, top: 4),
                                      child: FlatButton(
                                        onPressed: () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) {
                                                return CategoryPage(snapshot.data[1]);
                                              }
                                            )
                                          );
                                        },
                                        child: Text(
                                          'View All',
                                          style: TextStyle(color: Colors.green),
                                        ),
                                      )
                                    )
                                  )
                                ],
                              ),
                              _buildProductsCategory(snapshot.data[1])
                            ],
                          );
                        }
                    }
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCategoryList() {
    return Container(
      height: 150,
      alignment: Alignment.centerLeft,
      child: FutureBuilder(
        future: blocProduct.getCategories(),
        initialData: [],
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
              return Container(
                alignment: Alignment.center,
                width: 200.0,
                height: 200.0,
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white),
                  strokeWidth: 5.0,
                ),
              );
            default:
              if (snapshot.hasError)
                return Container();
              else {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    var data = snapshot.data[index];
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return CategoryPage(data);
                            }
                          )
                        );
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.all(10),
                              width: 95,
                              height: 95,
                              alignment: Alignment.center,
                              child: Image.network(
                                data.image,
                                height: 40,
                                width: 40,
                              ),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    offset: Offset(0, 5),
                                    blurRadius: 15,
                                  )
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  data.name,
                                  overflow: TextOverflow.ellipsis
                                ),
                              ],
                            )
                          ]
                        )
                      )
                    );
                  },
                );
              }
          }
        },
      )
    );
  }

  Widget _buildAllProducts() {
    return Container(
      height: 200,
      alignment: Alignment.centerLeft,
      child: StreamBuilder(
        stream: blocProduct.productsStream,
        initialData: [],
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
              return Container(
                alignment: Alignment.center,
                width: 200.0,
                height: 200.0,
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white),
                  strokeWidth: 5.0,
                ),
              );
            default:
              if (snapshot.hasError)
                return Container();
              else {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    var data = snapshot.data[index];
                    return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(10),
                            width: 130,
                            height: 140,
                            alignment: Alignment.center,
                            child: Image.network(
                              data.image,
                              fit: BoxFit.fill,
                            ),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(4),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  offset: Offset(0, 5),
                                  blurRadius: 15,
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: 130,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              data.name,
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 4, left: 4),
                            width: 130,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "R\$ ${data.price.toStringAsFixed(2)}",
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ]
                      );
                  },
                );

              }
          }
        },
      ),
    );
  }


  Widget _buildProductsCategory(CategoryModel cate) {

    return Container(
      height: 200,
      alignment: Alignment.centerLeft,
      child: FutureBuilder(
        future: blocProduct.getProductsByCategory(cate, 5),
        initialData: [],
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
              return Container(
                alignment: Alignment.center,
                width: 200.0,
                height: 200.0,
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white),
                  strokeWidth: 5.0,
                ),
              );
            default:
              if (snapshot.hasError)
                return Container();
              else {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    var data = snapshot.data[index];
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return Product(data);
                            }
                          )
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(10),
                            width: 130,
                            height: 140,
                            alignment: Alignment.center,
                            child: Image.network(
                              data.image,
                              fit: BoxFit.fill,
                            ),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(4),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  offset: Offset(0, 5),
                                  blurRadius: 15,
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: 130,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              data.name,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 4, left: 4),
                            width: 130,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "R\$ ${data.price.toStringAsFixed(2)}",
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ]
                      )
                    );
                  },
                );

              }
          }
        },
      ),
    );
  }

  // List<ProductModel> addItems(String type) {
  //   var list = List<ProductModel>();

  //   if (type == 'vestidos') {

  //     var data1 = ProductModel(name: 'Vestido Festa', image: 'https://ph-cdn3.ecosweb.com.br/Web/posthaus/foto/moda-feminina/vestido-curto//vestido-com-decote-transpassado-poa_215305_301_1.jpg', price: 300.00);
  //     list.add(data1);
  //     var data2 = ProductModel(name: 'Vestido Florido', image: 'https://assets.xtechcommerce.com/uploads/images/medium/c9097046d9ad715fb5de85be871a1c5f.jpg', price: 250.00);
  //     list.add(data2);
  //     var data3 = ProductModel(name: 'Vestido Longo', image: 'https://cdn-1.jjshouse.com/upimg/jjshouse/s1140/c6/b8/0133ade95497d67a40f8467f87c1c6b8.jpg', price: 500.00);
  //     list.add(data3);

  //   } else {

  //     var data3 = ProductModel(name: 'Camisa Social', image: 'https://images-americanas.b2w.io/produtos/01/00/img1/42757/9/42757991_1GG.jpg', price: 249.90);
  //     list.add(data3);
  //     var data4 = ProductModel(name: 'Camisa John John', image: 'https://img.elo7.com.br/product/zoom/1DD0E7B/camisas-camisetas-blusas-varias-marcas-famosas-renda-extra.jpg', price: 30.00);
  //     list.add(data4);
    
  //   }

  //   return list;
  // }

  List<CategoryModel> addCategories() {
    var list = List<CategoryModel>();

    var data1 = CategoryModel(name: 'Vestidos', image: 'https://static.vecteezy.com/system/resources/previews/000/512/461/non_2x/clothes-glyph-black-icon-vector.jpg');
    list.add(data1);
    var data2 = CategoryModel(name: 'Camisas', image: 'https://cdn0.iconfinder.com/data/icons/simple-mix-outline/160/t-shirt-512.png');
    list.add(data2);
    // var data3 = Category('House Hold', '');
    // list.add(data3);
    // var data4 = Category('House Hold', '');
    // list.add(data4);

    return list;
  }

  
}