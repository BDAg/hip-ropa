import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hipropa/blocs/product.dart';
import 'package:hipropa/blocs/user.dart';
import 'package:hipropa/models/product.dart';
import 'package:hipropa/pages/product.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {

  final _keyScaffold = new GlobalKey<ScaffoldState>();

  ProductBloc blocProduct;
  UserBloc blocUser;

  double total = 0.0;

  @override
  Widget build(BuildContext context) {
    this.blocProduct = BlocProvider.of<ProductBloc>(context);
    this.blocUser = BlocProvider.of<UserBloc>(context);

    return Scaffold(
      key: _keyScaffold,
      body: Stack(
        children: <Widget>[
          FutureBuilder(
            future: blocProduct.getCarrinho(blocUser.data),
            initialData: [],
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                case ConnectionState.none:
                  return Container(
                    alignment: Alignment.center,
                    width: 200.0,
                    height: 200.0,
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.white),
                      strokeWidth: 5.0,
                    ),
                  );
                default:
                  if (snapshot.hasError) {
                    return Container();
                  } else {
                    if (snapshot.data.length < 1) return Container(
                      child: Center(
                        child: Text("Sem Itens no Carrinho :("),
                      ),
                    );
                    else return ListView.separated(
                      itemCount: snapshot.data.length + 1,
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                      itemBuilder: (context, index){
                        if (index < snapshot.data.length) {
                          total += (snapshot.data[index]["qtd"] * snapshot.data[index]["produto"].price);
                          return Dismissible(
                            background: Container(
                              padding: EdgeInsets.only(left: 10.0),
                              color: Colors.red,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Icon(Icons.delete, size: 40.0, color: Colors.white),
                              ),
                            ),
                            key: UniqueKey(),
                            child: _listItem(snapshot.data[index]["produto"], snapshot.data[index]["qtd"]),
                            onDismissed: (dismissible) async {
                              await this.blocProduct.deleteItemCarrinho(this.blocUser.data, snapshot.data[index]["produto"], context, _keyScaffold);
                              setState(() {
                                
                              });
                            },
                          );
                        } else {
                          return Container(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            height: 100,
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "TOTAL: R\$ ${this.total.toStringAsFixed(2)}",
                                  style: TextStyle(
                                    fontSize: 25.0
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(right: 10.0),
                                      child: RaisedButton(
                                        onPressed: () async {
                                          await this.blocProduct.setOrder(this.blocUser.data, context, _keyScaffold);
                                          setState(() {
                                            total = 0.0;
                                          });
                                        },
                                        color: Colors.green,
                                        child: Text(
                                          "Fechar Compra",
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.white
                                          ),
                                        ),
                                      )
                                    ),
                                    RaisedButton(
                                      onPressed: () async {
                                        await this.blocProduct.deleteCarrinho(this.blocUser.data, context, _keyScaffold);
                                        setState(() {
                                          total = 0.0;
                                        });
                                      },
                                      color: Colors.red,
                                      child: Text(
                                        "Limpar Carrinho",
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.white
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            )
                          );
                        }
                      },
                    );
                  }
              }
            },
          )
        ],
      ),
    );
  }

  Widget _listItem(ProductModel product, int qtd) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      height: 120,
      // width: MediaQuery.of(context).size.height,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return Product(product);
              }
            )
          );
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          // mainAxisAlignment: MainAxisAlignment,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 40.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Image.network(
                  product.image,
                  height: 70,
                  fit: BoxFit.fill,
                )
              )
            ),
            Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      product.name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      softWrap: false
                    ),
                  ),
                  Text("${qtd.toString()} X R\$ ${product.price.toStringAsFixed(2)}")
                ],
              )
            ),
          ],
        ),
      ),
      // RaisedButton(
      //   onPressed: () {},
      //   color: Colors.red,
      //   child: Text(
      //     "Remover do Carrinho",
      //     style: TextStyle(
      //       fontSize: 10,
      //       color: Colors.white
      //     ),
      //   ),
      // )
    );
  }
}