import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hipropa/models/category.dart';
import 'package:hipropa/models/user.dart';
import 'package:hipropa/utils/api.dart';
import 'package:hipropa/models/product.dart';

import 'package:bloc_pattern/bloc_pattern.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class ProductBloc implements BlocBase {
  API api;

  List<ProductModel> _products;
  double temperatura = 0.0;

  Map<String, dynamic> cart = {};

  final _productController = StreamController.broadcast();

  Stream get productsStream => _productController.stream;
  List<ProductModel> get productsData => _products;
  Map get shoppingCart => cart;
  double get clima => temperatura;

  ProductBloc() {
    this.api = API();
    // this.getAllProducts();
  }

  setClima(double temp) {
    this.temperatura = temp;
  }

  checkTemp() async {

    while (true) {
      final temp = await this.getTemperatura();

      if (this.temperatura == 0.0) {
        print("Zerada");
        this.temperatura = temp;
      } else if (this.temperatura > temp || this.temperatura < temp) {
        print("mudou");
        this.temperatura = temp;
        _productController.add(true);
      } else {
        print("manteve");
      }

      await Future.delayed(new Duration(seconds: 5));

    }

  }

  getAllProducts() async {
    final response = await api.getAllProducts();

    List aux;

    if (response.statusCode == 200) {
      aux = json.decode(response.body).map((i) => ProductModel.fromJson(i)).toList();
    }

    // this._productController.add(this._products);
    return aux;
  }

  Future<List> getProductsByCategory(CategoryModel category, int limit) async {
    final response = await api.getProductsByCategory(category, limit);
 
    List aux;

    if (response.statusCode == 200) {
      aux = json.decode(response.body)['response'].map((i) => ProductModel.fromJson(i)).toList();
    }

    return aux;
  }

  Future<List> getCategoriesOFF() async {
    final response = await api.getCategoriesOFF();

    List aux;

    if (response.statusCode == 200) {
      aux = json.decode(response.body)['response'].map((i) => CategoryModel.fromJson(i)).toList();
    }

    return aux;
  }

  Future<List> getCategories() async {
    final response = await api.getCategories();

    List aux;

    if (response.statusCode == 200) {
      aux = json.decode(response.body)['response'].map((i) => CategoryModel.fromJson(i)).toList();
    }

    return aux;
  }

  Future getCarrinho(UserModel user) async {
    final response = await api.getShoppingCart(user);

    List aux;

    if (response.statusCode == 200) {
      aux = json.decode(response.body)["response"]["products"];
      this.cart = json.decode(response.body)["response"]["redis"];
      final produtos = aux.map( (i) => { "produto": ProductModel.fromJson(i), "qtd": int.parse(i["qtd"]) } ).toList();

      aux = produtos;
    }

    return aux;
  }

  setCarrinho(UserModel user, Map<String, dynamic> cartTemp, BuildContext context, GlobalKey<ScaffoldState> _keyScaffold) async {

    final idProduct = cartTemp.keys.first;

    print(this.cart);

    if (this.cart == null) {
      this.cart = {};
    }

    if (this.cart.containsKey(idProduct)) {
      this.cart[idProduct] = int.parse(this.cart[idProduct]) + cartTemp[idProduct];
    } else {
      this.cart[idProduct] = cartTemp[idProduct];
    }

    final response = await api.setShoppingCart(user, this.cart);

    if (response.statusCode == 200) {
      snackBarInfoError(context, _keyScaffold, "Inseriu no carrinho com Sucesso!");

    } else {
      snackBarInfoError(context, _keyScaffold, "Falhou ao inserir no carrinho!");
    
    }
  }

  deleteItemCarrinho(UserModel user, ProductModel product, BuildContext context, GlobalKey<ScaffoldState> _keyScaffold) async {

    this.cart.removeWhere((key, value) => key == product.id );

    final response = await api.setShoppingCart(user, this.cart);

    if (response.statusCode == 200) {
      snackBarInfoError(context, _keyScaffold, "Deletado do carrinho com Sucesso!");
    } else {
      snackBarInfoError(context, _keyScaffold, "Falha ao deletar do carrinho!");
    }

  }

  deleteCarrinho(UserModel user, BuildContext context, GlobalKey<ScaffoldState> _keyScaffold) async {
    this.cart = {};

    final response = await api.cleanShoppingCart(user);

    print(response.body);

    if (response.statusCode == 200) {
      snackBarInfoError(context, _keyScaffold, "Carrinho limpo com Sucesso!");
    } else {
      snackBarInfoError(context, _keyScaffold, "Falha ao limpar o carrinho!");
    }

  }

  setOrder(UserModel user, BuildContext context, GlobalKey<ScaffoldState> _keyScaffold) async {
    List products = [];

    for (final item in this.cart.keys) {
      Map aux = { "id_produto": item, "qtd": this.cart[item] };
      products.add(aux);
    }

    final response = await api.setOrder(user, products);

    print(response.body);

    if (response.statusCode == 200) {
      snackBarInfoError(context, _keyScaffold, "Carrinho limpo com Sucesso!");
      await api.cleanShoppingCart(user);
      this.cart = {};
      // Navigator.of(context).pop();
    } else {
      snackBarInfoError(context, _keyScaffold, "Falha ao limpar o carrinho!");
    }
  }

  Future getOrders(UserModel user) async {
    final response = await api.getOrders(user);

    List aux;

    if (response.statusCode == 200) { 
      aux = json.decode(response.body)["response"];

      for (final pedido in aux) {
        final produtos = pedido["produtos"].map( (i) => { "produto": ProductModel.fromJson(i["id_produto"]), "qtd": i["qtd"] } ).toList();
        pedido["produtos"] = produtos;
      }
    }

    return aux;

  }

  Future getTemperatura() async {
    final response = await api.getTemperatura();

    double temp = 0.0;

    if (response.statusCode == 200) {
      temp = json.decode(response.body)['response'][0]['temperatura'].toDouble();
    }

    return temp;
  }

  snackBarInfoError(BuildContext context, GlobalKey<ScaffoldState> _keyScaffold, String msg) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text(msg),
      backgroundColor: Colors.green,
      duration: Duration(seconds: 4),
    ));
  }

  @override
  void dispose() {
    _productController.close();
  }

}