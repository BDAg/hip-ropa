class UserModel {
  String id = '';
  String name = '';
  String email = '';
  String password = '';
  String token = '';

  UserModel({
    this.id,
    this.name,
    this.email,
    this.password,
    this.token,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return new UserModel(
      id: json["id"].toString(),
      name: json["name"],
      email: json["email"],
      password: json["password"],
      token: json["token"]
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "password": password,
    "token": token
  };

  Map<String, dynamic> toJsonLogin() => {
    "email": email,
    "senha": password
  };
  
  Map<String, dynamic> toJsonRegister() => {
    "nome": name,
    "email": email,
    "senha": password
  };
}