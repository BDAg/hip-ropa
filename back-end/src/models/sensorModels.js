var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var sensorSchema = new Schema({
    dataHora: { type: Date, default: Date.now },
    temperatura: { type: Number }
})

module.exports = mongoose.model("sensorTemperatura", sensorSchema, "sensorTemperatura");