var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var pedidosSchema = new Schema({
    id_cliente: { type: Schema.Types.ObjectId, ref: "clientes" },
    forma_pagamento: { type: String },
    data_pedido: { type: Date, default: Date.now },
    produtos: [ { id_produto: { type: Schema.Types.ObjectId, ref: "produtos" }, qtd: { type: Number } } ]
})

module.exports = mongoose.model("pedidos", pedidosSchema);