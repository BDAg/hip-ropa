var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var grupoProdutosSchema = new Schema({
    nomeGrupo: {type: String},
    tipoGrupo: {type: String},
    id_foto: {
        type: Schema.Types.ObjectId,
        ref: "imagens"
    }
})

module.exports = mongoose.model("grupos", grupoProdutosSchema);