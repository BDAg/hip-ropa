var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var clientesSchema = new Schema({
    nome: {type: String},
    email: {type: String, unique: true},
    senha: {type: String}
})

module.exports = mongoose.model("clientes", clientesSchema);