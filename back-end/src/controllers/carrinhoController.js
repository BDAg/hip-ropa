const jwt = require('jsonwebtoken');
const redis = require("redis");

var Produto = require("../models/produtosModels");

exports.insert = function (req, res, next) {

    let { token, carrinho } = req.body;

    if (token == undefined || carrinho == undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        let tokenDecoded = jwt.verify(token, "hipropa");

        let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
        conn.auth("Fatec2019#", (err, reply) => {
            if (err) {
                conn.end(true);
                res.status(500).json(
                    {
                        "response": null,
                        "error": err
                    }
                );
            } else {
                conn.del(tokenDecoded['_id'], (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {

                        carrinho = JSON.parse(carrinho);

                        conn.hmset(tokenDecoded['_id'], carrinho, (err, reply) => {
                        // conn.rpush(tokenDecoded['_id'], carrinho, (err, reply) => {
                            if (err) {
                                conn.end(true);
                                res.status(500).json(
                                    {
                                        "response": null,
                                        "error": err
                                    }
                                );
                            } else {
                                conn.end(true);
                                res.status(200).json({
                                    "error": null,
                                    "response": reply
                                });
                            }
                        });
                    }
                });
            }
        });

    }

}

exports.find = function(req, res, next) {

    const { token } = req.body;

    if (token == undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        let tokenDecoded = jwt.verify(token, "hipropa");

        let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
        conn.auth("Fatec2019#", (err, reply) => {
            if (err) {
                conn.end(true);
                res.status(500).json(
                    {
                        "response": null,
                        "error": err
                    }
                );
            } else {
                conn.hgetall(tokenDecoded['_id'], (err, reply) => {
                // conn.lrange(tokenDecoded['_id'], 0, -1, (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {

                        let produtos = [];

                        for (let item in reply) {
                            if (!produtos.includes(item)) produtos.push(item);
                        }

                        // produtos = reply.keys()

                        conn.end(true);
                        Produto.find({ _id : { $in : produtos } })
                            .populate({ path: 'id_grupo', populate: { path: 'id_foto' } })
                            .populate('id_foto')
                            .lean()
                            .exec(
                                (err, values) => {
                                    if (err) {
                                        res.status(500).json({
                                            'response': null,
                                            'error': err
                                        });
                                    } else {
                                        for (let produto of values) {
                                            for (let item in reply) {
                                                if (produto['_id'].toString() == item) {
                                                    produto['qtd'] = reply[item];
                                                    break;
                                                }
                                            }
                                        }
                                        res.status(200).json({
                                            'error': null,
                                            'response': {
                                                'products': values,
                                                'redis': reply
                                            }
                                        });
                                    }
                                }
                            );

                    }
                })

            }
        });

    }

}

exports.clear = function(req, res, next) {

    const { token } = req.body;

    if (token == undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        let tokenDecoded = jwt.verify(token, "hipropa");

        let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
        conn.auth("Fatec2019#", (err, reply) => {
            if (err) {
                conn.end(true);
                res.status(500).json(
                    {
                        "response": null,
                        "error": err
                    }
                );
            } else {
                conn.del(tokenDecoded['_id'], (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {
                        conn.end(true);
                        res.status(200).json({
                            "error": null,
                            "response": reply
                        });
                    }
                });
            }
        });
    }

}