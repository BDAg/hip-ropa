var Cliente = require("../models/clientesModels");
var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');

exports.findAllCliente = function (req,res) {
    Cliente.find({}, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.inserirCliente = function (req, res) {

    const { nome, email, senha } = req.body;

    if (nome == undefined || email == undefined || senha == undefined) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {

        bcrypt.hash(senha, 10, (err, passwordHash) => {
            if (err) {
                res.status(500).json({
                    'response': null,
                    'error': err
                });
            } else {
                const cliente = new Cliente({
                    'nome': nome,
                    'email': email,
                    'senha': passwordHash,
                })
                cliente.save((err, result) => {
                    if (err) {
                        res.status(406).json({
                            'response': null,
                            'error': err
                        });
                    } else {
                        let token = jwt.sign(
                            {
                                "name": result.nome,
                                "email": result.email,
                                "_id": result._id
                            },
                            "hipropa"
                        );
        
                        res.status(201).json({
                            'error': null,
                            'response': {
                                'auth': true,
                                'data': {
                                    '_id'  : result._id,
                                    'name' : result.nome,
                                    'email': result.email,
                                    'token': token,
                                },
                                'message': 'success'
                            }
                        });
                    }
                });
            }
    
        });
    }
};

exports.login = function(req, res) {
    const { email, senha } = req.body;

    if (email == undefined || senha == undefined) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {

        Cliente.findOne({ 'email': email }, (err, cliente) => {
            if (err) {
                res.status(500).json({
                    'response': null,
                    'error': err
                });
            } else {
    
                if (!cliente) {
                    res.status(200).json({
                        'error': null,
                        'response': {
                            'auth': false
                        }
                    });
                } else {
    
                    bcrypt.compare(senha, cliente.senha, (err, auth) => {
                        if (err) {
                            res.status(500).json({
                                'response': null,
                                'error': err
                            });
                        } else {
                            if (!auth) {
                                res.status(200).json({
                                    'error': null,
                                    'response': {
                                        'auth': auth
                                    }
                                });
                            } else {
    
                                let token = jwt.sign(
                                    {
                                        "name": cliente.nome,
                                        "email": cliente.email,
                                        "_id": cliente._id
                                    },
                                    "hipropa"
                                );
                                
                                res.status(201).json({
                                    'error': null,
                                    'response': {
                                        'auth': true,
                                        'data': {
                                            '_id'  : cliente._id,
                                            'name' : cliente.nome,
                                            'email': cliente.email,
                                            'token': token
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });

    }
}

exports.editarCliente = function (req,res) {
    let data = {
        _id: null,
        nome: null,
    };
    for (let i in req.body['data']) {
        if (i in data) {
            data[i] = req.body['data'][i];
        }
    }
    removeNull(data);
    
    Cliente.updateOne({
        _id: data['_id']
    }, data, function (err,raw) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            })
        }
        else {
            return res.status(200).json({
                'error': null,
                'response': {
                    'message': raw
                }
            })
        }
    }
    )
}

removeNull = function( data ) {
    for (let i in data) {
        if (data[i] == null) {
            delete data[i];
        }
    }
}

exports.buscaClienteByID = function (req, res) {
    Cliente.findById({
        _id: req.params.id
    }, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.buscaClienteByNome = function (req, res) {
    Cliente.find({
        nome: req.body.nome
    }, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.deletarCliente = function (req, res) {
    try {
        Cliente.deleteOne({
            _id: req.body.id,
        }, (err, result) => {
            if (err) {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            } else if (result.n == 1) {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    },
                })
            } else {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            }
        })
    } catch (error) {
        console.log("Erro")
    }
};