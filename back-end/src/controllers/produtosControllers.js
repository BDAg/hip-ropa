var Produto = require("../models/produtosModels");
var Imagem = require("../models/imagensModels");

exports.findAllProduto = function (req,res) {
    Produto.find({}).populate('id_grupo').populate('id_foto').exec((err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

// exports.findAllDetailsProdutos = function (req,res) {
//     Produto.find({}).populate({ path: 'id_foto'}).exec((err, values) => {
//         if (err) {
//             res.status(406).json({
//                 'response': null,
//                 'error': err
//             });
//         } else {
//             res.status(200).json({
//                 "dados": values
//             });
//         }
//     });
// };

exports.inserirProduto = function (req, res) {
    const { nome, preco, id_grupo, href } = req.body;

    if (nome == undefined || preco == undefined || id_grupo == undefined || href == undefined) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {

        const imagem = new Imagem({
            hrefFoto: href
        });
    
        imagem.save((err, result) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                })
            } else {

                const produto = new Produto({
                    'nome': nome,
                    'preco': preco,
                    'id_grupo': id_grupo,
                    'id_foto': result._id,
                });
        
                produto.save((err, result) => {
                    if (err) {
                        res.status(406).json({
                            'response': null,
                            'error': err
                        })
                    } else {
                        res.status(200).json({
                            'error': null,
                            'response': {
                                'message': 'success'
                            }
                        });
                    }
                });
        
            }
        });
    }
};

exports.editarProduto = function (req,res) {
    let data = {
        _id: null,
        nome: null,
    };
    for (let i in req.body['data']) {
        if (i in data) {
            data[i] = req.body['data'][i];
        }
    }
    removeNull(data);
    
    Produto.updateOne({
        _id: data['_id']
    }, data, function (err,raw) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            })
        }
        else {
            return res.status(200).json({
                'error': null,
                'response': {
                    'message': raw
                }
            })
        }
    }
    )
}

removeNull = function( data ) {
    for (let i in data) {
        if (data[i] == null) {
            delete data[i];
        }
    }
}

exports.buscaProdutoByID = function (req, res) {

    const idGroup = req.params.id_group;
    const limite = req.params.limite;

    if (idGroup == undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {
        Produto.find({
            id_grupo: idGroup
        })
        .limit(Number(limite).valueOf())
        .populate({ path: 'id_grupo', populate: { path: 'id_foto' }})
        .populate('id_foto')
        .exec((err, result) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                })
            } else {
                res.status(200).json({
                    'error': null,
                    'response': result
                })
            }
        })
    }
};

exports.buscaProdutoByNome = function (req, res) {
    Produto.find({
        nome: req.body.nome
    }, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.deletarProduto = function (req, res) {
    try {
        Produto.deleteOne({
            _id: req.body.id,
        }, (err, result) => {
            if (err) {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            } else if (result.n == 1) {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    },
                })
            } else {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            }
        })
    } catch (error) {
        console.log("Erro")
    }
};