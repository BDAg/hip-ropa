var Grupo = require("../models/gruposModels");
var Imagem = require("../models/imagensModels");
var Sensor = require("../models/sensorModels");

exports.findAllGrupo = function (req,res) {
    Grupo.find({}).populate('id_foto').exec((err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.getOff = function(req, res) {
    Sensor
    .find()
    .sort({ dataHora: -1 })
    .limit(1)
    .exec(
        (err, values) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (!values) {
                    Grupo
                    .find({})
                    .limit(2)
                    .populate('id_foto')
                    .exec((err, result) => {
                        if (err) {
                            res.status(406).json({
                                'response': null,
                                'error': err
                            })
                        } else {
                            res.status(200).json({
                                'error': null,
                                'response': result
                            })
                        }
                    })
                } else {
                    if (values[0]['temperatura'] < 20) {
                        Grupo
                        .find({ _id: { $in : [ "5ddd5d674d884901ee10e110", "5ddd584cb5b626b34acec1f9" ] } })
                        .populate('id_foto')
                        .exec((err, result) => {
                            if (err) {
                                res.status(406).json({
                                    'response': null,
                                    'error': err
                                })
                            } else {
                                res.status(200).json({
                                    'error': null,
                                    'response': result
                                })
                            }
                        });
                    } else if (values[0]['temperatura'] >= 20 && values[0]['temperatura'] < 25) {
                        Grupo
                        .find({ _id: { $in : [ "5ddc11d49eab1f6f7226a326", "5ddc19d7e474d71ce6835812" ] } })
                        .populate('id_foto')
                        .exec((err, result) => {
                            if (err) {
                                res.status(406).json({
                                    'response': null,
                                    'error': err
                                })
                            } else {
                                res.status(200).json({
                                    'error': null,
                                    'response': result
                                })
                            }
                        });
                    } else {
                        Grupo
                        .find({ _id: { $in : [ "5ddd632c5d7964f89bfb06db", "5ddd690c5d7964f89bfb0fce" ] } })
                        .populate('id_foto')
                        .exec((err, result) => {
                            if (err) {
                                res.status(406).json({
                                    'response': null,
                                    'error': err
                                })
                            } else {
                                res.status(200).json({
                                    'error': null,
                                    'response': result
                                })
                            }
                        });
                    }
                }
            }
        }
    )
}

exports.findAllDetailsGrupo = function (req,res) {
    Grupo.find({}).populate({ path: 'id_foto', populate: { path: 'id_foto' } }).exec((err, values) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
            res.status(200).json({
                "dados": values
            });
        }
    });
};

exports.inserirGrupo = function (req, res) {

    const { nomeGrupo, tipoGrupo, hrefFoto } = req.body;

    if (nomeGrupo == undefined || tipoGrupo == undefined || hrefFoto == undefined) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {
        const imagem = new Imagem({
            hrefFoto: hrefFoto
        });
    
        imagem.save((err, result) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                })
            } else {
    
                const grupo = new Grupo({
                    nomeGrupo: nomeGrupo,
                    tipoGrupo: tipoGrupo,
                    id_foto: result._id,
                })
                grupo.save((err, result) => {
                    if (err) {
                        res.status(406).json({
                            'response': null,
                            'error': err
                        })
                    } else {
                        res.status(200).json({
                            'error': null,
                            'response': {
                                'message': 'success'
                            }
                        });
                    }
                })
    
            }
        });   
    }
};

exports.editarGrupo = function (req,res) {
    let data = {
        _id: null,
        nomeGrupo: null,
    };
    for (let i in req.body['data']) {
        if (i in data) {
            data[i] = req.body['data'][i];
        }
    }
    removeNull(data);
    
    Grupo.updateOne({
        _id: data['_id']
    }, data, function (err,raw) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            })
        }
        else {
            return res.status(200).json({
                'error': null,
                'response': {
                    'message': raw
                }
            })
        }
    }
    )
}

removeNull = function( data ) {
    for (let i in data) {
        if (data[i] == null) {
            delete data[i];
        }
    }
}

exports.buscaGrupoByID = function (req, res) {
    Grupo.findById({
        _id: req.body.id
    }).lean().exec(async (err, grupo) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            let groups = [];
            let images = [];

            groups.push(grupo);
            if (!(grupo['idFoto'] in images)) {
                images.push(grupo['idFoto']);
            }
            Imagem.find({ _id: { $in: images } }, function(err, images) {
                if (err) {
                    res.status(406).json({
                        'response': null,
                        'error': err
                    })
                } else {
                    for (let index in groups) {
                        let idImage = groups[index]['idFoto'];
                        for (image of images) {
                            if (image._id.toString() == idImage.toString()) {
                                groups[index]['hrefFoto'] = image['hrefFoto'];
                                break;
                            }
                        }
                    }
                    return res.status(200).json({
                        'response': groups
                    });
                }
            });
        }
    })
};

exports.buscaGrupoByNome = function (req, res) {
    Grupo.find({
        nomeGrupo: req.body.nomeGrupo
    }, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.deletarGrupo = function (req, res) {
    try {
        Grupo.deleteOne({
            _id: req.body.id,
        }, (err, result) => {
            if (err) {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            } else if (result.n == 1) {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    },
                })
            } else {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            }
        })
    } catch (error) {
        console.log("Erro")
    }
};