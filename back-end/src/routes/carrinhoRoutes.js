const express = require("express");
const router = express.Router();

let carrinhoController = require('../controllers/carrinhoController');

router.post('/insert', carrinhoController.insert);
router.post('/find', carrinhoController.find);
router.post('/clear', carrinhoController.clear);

module.exports = router