const express = require("express");
const router = express.Router();
var imagemController = require('../controllers/imagensControllers');

router.get("/buscar", imagemController.findAllImagem)
router.post("/buscar/:id", imagemController.buscaImagemByID)
router.post("/buscar/nome/", imagemController.buscaImagemByNome)
router.post("/inserir/", imagemController.inserirImagem)
router.post("/editar/", imagemController.editarImagem)
router.post("/deletar/",imagemController.deletarImagem)

module.exports = router