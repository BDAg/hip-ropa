import requests
from bs4 import BeautifulSoup
import csv
import pymongo
from pymongo import MongoClient

conexao = pymongo.MongoClient('mongodb+srv://admHipRopa:bc123456@hip-ropa-s9hbg.mongodb.net')
mydb = conexao['HipRopa']

def getIdFoto(mydb,hrefFoto):
    buscaFoto = mydb.imagens.find_one({
        'hrefFoto' : hrefFoto
    })

    if buscaFoto != None:
        idFoto = buscaFoto['_id']

    else:
        idFoto = mydb.imagens.insert({
            'hrefFoto' : hrefFoto
        })

    return idFoto

def insertRoupas(mydb,categoria,nome,descricao,preco,foto,tipoCategoria):
    preco = str(preco).replace('R$','').replace(',','.').strip()

    busca = mydb.produtos.find_one({
        'nome' : descricao,
        'descricao' : str(nome) + ' - ' + str(descricao),
    })

    if busca == None:
        buscaCategoria = mydb.grupos.find_one({
            'nomeGrupo' : categoria,
            'tipoGrupo' : tipoCategoria
        })

        if buscaCategoria != None:
            idCategoria = buscaCategoria['_id']
        
        else:
            idFotoGrupo = getIdFoto(mydb,'https://cdn2.iconfinder.com/data/icons/rounded-white-basic-ui-set-3/139/List-RoundedWhite-2-512.png')

            idCategoria = mydb.grupos.insert({
                'nomeGrupo' : categoria,
                'tipoGrupo' : tipoCategoria,
                'id_foto' : idFotoGrupo
            })

        idFotoProduto = getIdFoto(mydb,foto)

        if (len(preco.split('.')) > 2):
            preco = preco[::-1].split('.', 1)[1][::-1]

        insertProduct = mydb.produtos.insert({
            'nome' : descricao,
            'descricao' : str(nome) + ' - ' + str(descricao),
            'preco' : float(preco),
            'id_grupo' : idCategoria,
            'id_foto' : idFotoProduto
        })       

        print ('Insert : '+str(insertProduct))
    else:
        print ('Found : '+str(busca['_id']))

vnome = []
vdescricao = []
vpreco = []
vfoto = []

# categorias = [{'url':'roupas-masculinas','tipo' : 'Roupa Masculina'},{'url':'roupas-femininas','tipo' : 'Roupa Feminina'},{'url':'roupas-infantis','tipo' : 'Roupa Infantil'}]
# categorias = [
#     {
#         'url'  : 'roupas-masculinas/casacos-e-jaquetas',
#         'tipo' : 'Malha e Suéter Masculino'
#     },
#     {
#         'url'  : 'roupas-femininas/malhas-e-sueters',
#         'tipo' : 'Malha e Suéter Feminino'
#     }
# ]

categorias = [
    {
        'url'  : 'roupas-femininas/shorts',
        'tipo' : 'Short e Bermuda Feminino'
    },
    {
        'url'  : 'roupas-masculinas/bermudas',
        'tipo' : 'Bermuda Masculina'
    }
]

for catMal in categorias:
    contador = 1
    while contador < 28:
        requisicao = requests.get("https://www.dafiti.com.br/"+str(catMal['url'])+"/?page="+str(contador))
        soup = BeautifulSoup(requisicao.content, 'html.parser')

        nomeproduto = soup.find_all('div',{'class':'product-box-brand'})
        for linha in nomeproduto: 
            nome = linha.text
            vnome.append(nome)

        descricaoproduto = soup.find_all('p',{'class':'product-box-title hide-mobile'})
        for linhad in descricaoproduto:
            descricao = linhad.text
            vdescricao.append(descricao)

        precoproduto = soup.find_all('span', {'class', 'product-box-price-from'})
        for linhap in precoproduto:
            preco = linhap.text
            vpreco.append(preco)
        
        categoria = soup.select_one("div.text-box-brand-campaign h1").text

        fotoproduto = soup.select('a.product-box-link.is-lazyloaded.image.product-image-rotate img')
        for linhap in fotoproduto:
            foto = linhap['data-toggle-img']
            vfoto.append(foto)

        for nome,desc,preco,foto in zip(vnome,vdescricao,vpreco, vfoto):
            inserir = (str(categoria)+';'+str(nome)+';'+str(desc)+';'+str(preco)+';'+str(foto)+'\n')
            insertRoupas(mydb,categoria,nome,desc,preco,foto,catMal['tipo'])

        print("Categoria : {}\nPágina : {} \n\n".format(categoria,contador))

        contador+=1

        vdescricao.clear()
        vnome.clear()
        vfoto.clear()